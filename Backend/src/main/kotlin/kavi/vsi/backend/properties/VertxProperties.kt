package kavi.vsi.backend.properties

import io.vertx.core.json.JsonObject
import org.springframework.boot.context.properties.ConfigurationProperties


/**
 * Vertx属性配置
 * */
@ConfigurationProperties(prefix = VertxProperties.PREFIX, ignoreUnknownFields = true)
open class VertxProperties (
        open var enabledCluster: Boolean? = false, // 是否集群模式
        open var vertxTimeout: Long = 10 // 启动阻塞等待超时时间(秒)
) {
    /**
     * 实体类转JSON类型
     * */
    fun toJson(): JsonObject = JsonObject.mapFrom(this);

    companion object{
        const val PREFIX = "vertx"
    }
}