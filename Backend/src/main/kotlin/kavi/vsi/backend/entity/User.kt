package kavi.vsi.backend.entity

import org.springframework.data.relational.core.mapping.Table

/**
 * 用户基础信息
 */
@Table(User.tableName)
class User {
    /* 用户ID */
    var id: Long? = null

    /* 用户名 */
    var username: String? = null

    /* 密码 */
    var password: String? = null

    companion object {
        /* 表名 */
        const val tableName = "system_user"
    }
}