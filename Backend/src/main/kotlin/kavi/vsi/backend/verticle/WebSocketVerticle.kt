package kavi.vsi.backend.verticle

import io.vertx.core.AsyncResult
import io.vertx.reactivex.core.eventbus.EventBus
import io.vertx.core.http.HttpMethod
import io.vertx.core.http.HttpServerOptions
import io.vertx.core.json.JsonObject
import io.vertx.ext.bridge.PermittedOptions
import io.vertx.ext.web.handler.sockjs.SockJSBridgeOptions
import io.vertx.reactivex.core.AbstractVerticle
import io.vertx.reactivex.core.http.HttpServer
import io.vertx.reactivex.ext.web.Router
import io.vertx.reactivex.ext.web.handler.BodyHandler
import io.vertx.reactivex.ext.web.handler.CorsHandler
import io.vertx.reactivex.ext.web.handler.StaticHandler
import io.vertx.reactivex.ext.web.handler.sockjs.BridgeEvent
import io.vertx.reactivex.ext.web.handler.sockjs.SockJSHandler
import org.slf4j.LoggerFactory
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Service

@Service
class WebSocketVerticle : AbstractVerticle() {

    private val log = LoggerFactory.getLogger(this.javaClass)

    @Autowired
    private lateinit var eb: EventBus

    @Throws(Exception::class)
    override fun start() {
        super.start()
        val router = Router.router(vertx)
        val corsHandler = CorsHandler.create("*")
                .allowCredentials(true)
                .allowedHeader("Access-Control-Allow-Origin")
                .allowedHeader("Access-Control-Allow-Headers")
                .allowedHeader("Content-Type")
                .allowedHeader("origin")
                .allowedHeader("accept")
                .allowedMethod(HttpMethod.GET)
                .allowedMethod(HttpMethod.POST)
                .allowedMethod(HttpMethod.PUT)
                .allowedMethod(HttpMethod.DELETE)
        router.route("/*").handler(corsHandler)

        // Socket接口地址
        router.route("/socket/*").handler(this.webSocketHandler())

        val options = HttpServerOptions()
        options.port = 8083
        vertx.createHttpServer(options)
            .requestHandler(router)
            .listen { result: AsyncResult<HttpServer> ->
                if (result.succeeded()) {
                    log.info("[Success] start sdk port: " + result.result().actualPort())
                } else {
                    log.error("[Failed] to sdk", result.cause())
                }
            }
    }

    /**
     * APP WebSocket 处理
     */
    private fun webSocketHandler(): SockJSHandler? {
        val ebHandler = SockJSHandler.create(vertx)
        val bridgeOptions = SockJSBridgeOptions()

        this.eb.consumer<JsonObject>("vc.demo.test"){

        }

        // 消息监听权限设置
        val options = PermittedOptions().setAddressRegex("vc.demo.*")
        bridgeOptions.addOutboundPermitted(options)
        bridgeOptions.addInboundPermitted(options)
        // 桥接
        ebHandler.bridge(bridgeOptions) { event: BridgeEvent ->
            // todo 数据监听
            event.complete(true)
        }
        return ebHandler
    }
}