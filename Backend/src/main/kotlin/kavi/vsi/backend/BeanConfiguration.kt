package kavi.vsi.backend

import io.vertx.core.AsyncResult
import io.vertx.core.VertxOptions
import io.vertx.reactivex.core.Vertx
import io.vertx.reactivex.core.eventbus.EventBus
import kavi.vsi.backend.properties.VertxProperties
import org.springframework.boot.context.properties.EnableConfigurationProperties
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration
import java.util.concurrent.CompletableFuture
import java.util.concurrent.TimeUnit

@Configuration
@EnableConfigurationProperties(VertxProperties::class)
class BeanConfiguration {
    /**
     * Vertx 服務初始化
     */
    @Bean
    @Throws(Exception::class)
    fun vertx(vertxProperties: VertxProperties): Vertx {
        val options = VertxOptions(vertxProperties.toJson())

        if (vertxProperties.enabledCluster != true) {
            return Vertx.vertx(options)
        }
        val future: CompletableFuture<Vertx> = CompletableFuture<Vertx>()
        Vertx.clusteredVertx(options) { ar: AsyncResult<Vertx?> ->
            if (ar.succeeded()) {
                future.complete(ar.result())
            } else {
                future.completeExceptionally(ar.cause())
            }
        }
        // 等待2分钟超时
        return future.get(vertxProperties.vertxTimeout, TimeUnit.SECONDS)
    }

    /**
     * 消息总线
     */
    @Bean
    fun eventBus(vertx: Vertx): EventBus {
        return vertx.eventBus()
    }

}