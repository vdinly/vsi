package kavi.vsi.gateway.webflux;

import org.springframework.web.reactive.HandlerResult;
import org.springframework.web.server.ServerWebExchange;
import reactor.core.CorePublisher;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;
import reactor.util.annotation.Nullable;

/**
 * 输出封装接口
 * */
public interface ResponseWrapHandler {

    /**
     * @param body 多个元素内容
     * @param exchange 请求附加属性
     * @param result 处理程序方法
     * @return 返回结果
     * */
    @Nullable
    CorePublisher<Object> result(Flux<Object> body, ServerWebExchange exchange, HandlerResult result);

    /**
     * @param body 元素内容
     * @param exchange 请求附加属性
     * @param result 处理程序方法
     * @return 返回结果
     * */
    @Nullable
    CorePublisher<Object> result(@Nullable Mono<Object> body, ServerWebExchange exchange, HandlerResult result);

    /**
     * 调度器
     * @param exchange 请求附加属性
     * @param result 处理程序方法
     * */
    @SuppressWarnings("unchecked")
    default Object handler(ServerWebExchange exchange, HandlerResult result) {
        Object returnValue = result.getReturnValue();
        if (returnValue instanceof Mono) {
            return result(((Mono<Object>) returnValue), exchange, result);
        } else if (returnValue instanceof Flux) {
            return result(((Flux<Object>) returnValue), exchange, result);
        }
        return result(returnValue != null ? Mono.just(returnValue) : null, exchange, result);
    }
}
