package kavi.vsi.gateway.webflux;

import com.sun.istack.internal.NotNull;

import java.lang.annotation.*;

/**
 * 输出封装自定义处理器注解
 * */

@Target({ElementType.TYPE, ElementType.METHOD})
@Retention(RetentionPolicy.RUNTIME)
@Documented
public @interface ResponseWrap {
    /**
     * 自定义类须继承 ResponseWrapHandler.class
     * */
    @NotNull Class<? extends ResponseWrapHandler> value();
}
