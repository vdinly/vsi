package kavi.vsi.gateway.webflux;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.core.MethodParameter;
import org.springframework.core.ReactiveAdapterRegistry;
import org.springframework.core.annotation.AnnotationUtils;
import org.springframework.http.codec.HttpMessageWriter;
import org.springframework.web.reactive.HandlerResult;
import org.springframework.web.reactive.accept.RequestedContentTypeResolver;
import org.springframework.web.reactive.result.method.annotation.ResponseBodyResultHandler;
import org.springframework.web.server.ServerWebExchange;
import reactor.core.publisher.Mono;
import java.util.List;

/**
 * 重定义 ResponseBodyResultHandler
 * */
public class WebFluxResponseBodyResultHandler extends ResponseBodyResultHandler {

    private Logger log = LoggerFactory.getLogger(this.getClass());

    public WebFluxResponseBodyResultHandler(List<HttpMessageWriter<?>> writers,
                                            RequestedContentTypeResolver resolver,
                                            ReactiveAdapterRegistry registry) {
        super(writers, resolver, registry);
    }

    /**
     * 默认封装
     * */
    private DefaultResponseWrapHandler defaultResponseWrapResult = new DefaultResponseWrapHandler();

    /**
     * 重新处理输出结果
     * */
    @Override
    public Mono<Void> handleResult(ServerWebExchange exchange, HandlerResult result) {
        MethodParameter returnType = result.getReturnTypeSource();
        Class<?> containingClass = returnType.getContainingClass();
        ResponseWrap clazz = AnnotationUtils.getAnnotation(containingClass, ResponseWrap.class);
        if (clazz != null) {
            try {
                Object body = clazz.value().newInstance().handler(exchange, result);
                return writeBody(body, returnType, exchange);
            } catch (Exception e){
                log.error("ResponseWrapHandler class execution failed", e);
            }
        }
        Object body = defaultResponseWrapResult.handler(exchange, result);
        return writeBody(body, returnType, exchange);
    }

}
