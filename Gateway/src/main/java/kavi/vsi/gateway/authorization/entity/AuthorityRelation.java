package kavi.vsi.gateway.authorization.entity;

import org.springframework.data.relational.core.mapping.Table;

/**
 * 角色权限关系表
 * */
@Table(AuthorityRelation.tableName)
public class AuthorityRelation {
    /* 表名 */
    public final static String tableName = "system_authority_relation";

    /* 父级 */
    private String parent;
    /* 子项 */
    private String child;
    /* 是否已删除 */
    private Long deleted_at;

    public String getParent() {
        return parent;
    }
    public void setParent(String parent) {
        this.parent = parent;
    }

    public String getChild() {
        return child;
    }
    public void setChild(String child) {
        this.child = child;
    }

    public long getDeleted_at() {
        return deleted_at;
    }
    public void setDeleted_at(Long deleted_at) {
        this.deleted_at = deleted_at;
    }
}