package kavi.vsi.gateway.authorization.entity;

import org.springframework.data.relational.core.mapping.Table;

/**
 * 用户基础信息
 * */
@Table(User.tableName)
public class User {
    /* 表名 */
    public final static String tableName = "system_user";


    /* 用户ID */
    private Long id;
    /* 用户名 */
    private String username;
    /* 密码 */
    private String password;

    public long getId() {
        return this.id;
    }
    public void setId(Long id) {
        this.id = id;
    }

    public String getUsername() {
        return this.username;
    }
    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return this.password;
    }
    public void setPassword(String password) {
        this.password = password;
    }
}
