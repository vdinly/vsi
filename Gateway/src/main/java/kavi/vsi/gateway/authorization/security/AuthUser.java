package kavi.vsi.gateway.authorization.security;

import kavi.vsi.gateway.authorization.entity.User;
import org.springframework.security.core.GrantedAuthority;
import java.util.Collection;

/**
 * 重构授权用户信息
 * */
public class AuthUser extends org.springframework.security.core.userdetails.User {

    /**
     * 用户信息
     * */
    private User user;

    public User getUser() {
        return this.getUser();
    }
    public void setUser(User user) {
        this.user = user;
    }

    public AuthUser(User user, Collection<? extends GrantedAuthority> authorities) {
        super(user.getUsername(), user.getPassword(), true, true, true, true, authorities);
        this.setUser(user);
    }

    public AuthUser(User user, boolean enabled,
                    boolean accountNonExpired, boolean credentialsNonExpired,
                    boolean accountNonLocked, Collection<? extends GrantedAuthority> authorities) {
        super(user.getUsername(), user.getPassword(), enabled, accountNonExpired, credentialsNonExpired, accountNonLocked, authorities);
        this.setUser(user);
    }
}
