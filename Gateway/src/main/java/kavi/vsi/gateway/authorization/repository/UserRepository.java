package kavi.vsi.gateway.authorization.repository;

import kavi.vsi.gateway.authorization.entity.User;
import org.springframework.data.r2dbc.repository.Query;
import org.springframework.data.r2dbc.repository.R2dbcRepository;
import reactor.core.publisher.Mono;


/**
 * 用户表存储过程
 * */
public interface UserRepository extends R2dbcRepository<User, Long> {

    @Query("SELECT * FROM " + User.tableName + " WHERE username = :username LIMIT 1")
    Mono<User> findByUserName(String username);
}