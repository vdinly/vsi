package kavi.vsi.gateway.authorization.service;

import reactor.core.publisher.Mono;

import java.util.List;

public interface AuthorizationServiceInterface {

    /**
     * 根据用户ID获取所有授权（角色/权限）
     * @param userId 用户ID
     * */
    Mono<List<String>> authorizationByUserId(Long userId);
}
