package kavi.vsi.gateway.authorization.security;

import org.springframework.security.core.Authentication;
import org.springframework.security.web.server.WebFilterExchange;
import org.springframework.security.web.server.authentication.ServerAuthenticationSuccessHandler;
import reactor.core.publisher.Mono;

/**
 * 表单登入成功获取权限信息
 * */
public class FormAuthenticationSuccessHandler implements ServerAuthenticationSuccessHandler {

    /**
     * @return 执行handler的url 或
     * 者直接返回结果 return exchange.getResponse().writeWith(Mono.just(exchange.getResponse().bufferFactory().wrap("body".getBytes())));
     * */
    @Override
    public Mono<Void> onAuthenticationSuccess(WebFilterExchange exchange,
                                              Authentication authentication) {
        // 执行下一个 对应的 URL 页面
       return exchange.getChain().filter(exchange.getExchange());
    }
}
