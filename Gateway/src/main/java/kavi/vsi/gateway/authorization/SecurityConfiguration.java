package kavi.vsi.gateway.authorization;


import kavi.vsi.gateway.authorization.security.ExceptionAuthenticationEntryPoint;
import kavi.vsi.gateway.authorization.security.FormAuthenticationSuccessHandler;
import kavi.vsi.gateway.authorization.security.ReactiveUserDetailsServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.authentication.DelegatingReactiveAuthenticationManager;
import org.springframework.security.authentication.ReactiveAuthenticationManager;
import org.springframework.security.authentication.UserDetailsRepositoryReactiveAuthenticationManager;
import org.springframework.security.config.annotation.method.configuration.EnableReactiveMethodSecurity;
import org.springframework.security.config.annotation.web.reactive.EnableWebFluxSecurity;
import org.springframework.security.config.web.server.ServerHttpSecurity;
import org.springframework.security.core.userdetails.ReactiveUserDetailsService;
import org.springframework.security.web.server.SecurityWebFilterChain;
import org.springframework.security.web.server.authentication.ServerAuthenticationSuccessHandler;
import reactor.core.publisher.Mono;

import java.util.LinkedList;

@Configuration
@EnableWebFluxSecurity // 启用webflux登陆权限校验
@EnableReactiveMethodSecurity // 启用@PreAuthorize注解配置
@EnableConfigurationProperties({ SecurityProperties.class })
public class SecurityConfiguration {

    /**
     * 引入权限配置参数
     * */
    @Autowired
    SecurityProperties securityProperties;

    /**
     * 数据库用户权限信息
     * */
    @Bean
    public ReactiveUserDetailsService userDetailsService() {
        return new ReactiveUserDetailsServiceImpl();
    }

    /**
     * 表单登入成功获取权限信息
     * */
    @Bean
    ServerAuthenticationSuccessHandler formAuthenticationSuccessHandler() {
        return new FormAuthenticationSuccessHandler();
    }

    /**
     * 权限信息验证管理
     * */
    @Bean
    ReactiveAuthenticationManager reactiveAuthenticationManager() {
        LinkedList<ReactiveAuthenticationManager> managers = new LinkedList<>();
        managers.add(authentication -> {
            // 其他登陆方式 (比如手机号验证码登陆) 可在此设置,不得抛出异常或者 Mono.error
            return Mono.empty();
        });
        // 必须放最后不然会优先使用用户名密码校验但是用户名密码不对时此 AuthenticationManager 会调用 Mono.error 造成后面的 AuthenticationManager 不生效
        managers.add(new UserDetailsRepositoryReactiveAuthenticationManager(this.userDetailsService()));
        return new DelegatingReactiveAuthenticationManager(managers);
    }

    /**
     * 访问过滤器
     * */
    @Bean
    SecurityWebFilterChain webFilterChain(ServerHttpSecurity http) {
        return http
                .authorizeExchange(this::authorizeExchange)
                .csrf().disable() // 禁用CSRF
                .httpBasic().disable() // 禁用HTTP头部进行认证
                .formLogin(this::formLogin) // 表单验证
                .exceptionHandling().authenticationEntryPoint(new ExceptionAuthenticationEntryPoint()) // 验证失败错误处理
                .and()
//                .securityContextRepository() // 自定义存储
                .build();
    }

    /**
     * 权限访问控制
     * */
    private void authorizeExchange(ServerHttpSecurity.AuthorizeExchangeSpec exchanges) {
        //无需进行权限过滤的请求路径
        String[] allowUris = (securityProperties.getAllowPaths()).toArray(new String[0]);
        if (allowUris.length > 0) exchanges.pathMatchers(allowUris).permitAll();
        exchanges.anyExchange().authenticated();
    }

    /**
     * 表单登入验证配置
     * */
    private void formLogin(ServerHttpSecurity.FormLoginSpec login) {
        login.authenticationFailureHandler((exchange, exception) -> Mono.error(exception)); // 验证失败直接返回错误信息
        login.authenticationSuccessHandler(this.formAuthenticationSuccessHandler());
    }
}
