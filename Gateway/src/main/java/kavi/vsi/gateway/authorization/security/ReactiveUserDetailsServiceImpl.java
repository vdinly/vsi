package kavi.vsi.gateway.authorization.security;


import kavi.vsi.gateway.authorization.repository.UserRepository;
import kavi.vsi.gateway.authorization.service.AuthorizationService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.authority.AuthorityUtils;
import org.springframework.security.core.userdetails.ReactiveUserDetailsService;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.util.Assert;
import reactor.core.publisher.Mono;


/**
 * 获取用户权限信息
 * */
public class ReactiveUserDetailsServiceImpl implements ReactiveUserDetailsService {

    @Autowired
    UserRepository userRepository;

    @Autowired
    AuthorizationService authorizationService;

    @Override
    public Mono<UserDetails> findByUsername(String username){
        return userRepository.findByUserName(username)
                .switchIfEmpty(Mono.error(new UsernameNotFoundException(username)))
                .doOnNext(user -> {
                    Assert.notNull(user.getUsername(), "Username is null");
                    Assert.notNull(user.getPassword(), "Password is null");
                })
                .flatMap(user ->
                    authorizationService
                    .authorizationByUserId(user.getId())
                    .map(list ->
                        new AuthUser(user, AuthorityUtils.createAuthorityList(list.toArray(new String[0])))
                    )
                );
    }
}
