package kavi.vsi.gateway.authorization.security;

import org.springframework.http.HttpStatus;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.web.server.ServerAuthenticationEntryPoint;
import org.springframework.web.server.ResponseStatusException;
import org.springframework.web.server.ServerWebExchange;
import reactor.core.publisher.Mono;

/**
 * 授权入口错误处理
 * */
public class ExceptionAuthenticationEntryPoint implements ServerAuthenticationEntryPoint {
    @Override
    public Mono<Void> commence(ServerWebExchange exchange, AuthenticationException exception) {
        // 限制所有未授权通过请求返回403状态错误
        return Mono.error(new ResponseStatusException(HttpStatus.FORBIDDEN, exception.getMessage(), exception));
    }
}
