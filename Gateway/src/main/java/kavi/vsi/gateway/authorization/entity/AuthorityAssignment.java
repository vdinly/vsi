package kavi.vsi.gateway.authorization.entity;

import org.springframework.data.relational.core.mapping.Table;

/**
 * 用户角色权限分配表
 * */
@Table(AuthorityAssignment.tableName)
public class AuthorityAssignment {
    /* 表名 */
    public final static String tableName = "system_authority_assignment";


    /* 用户ID */
    private Long user_id;
    /* 用户对应角色或权限 */
    private String authority_name;
    /* 是否已删除 */
    private Long deleted_at;

    public long getUser_id() {
        return user_id;
    }
    public void setUser_id(Long user_id) {
        this.user_id = user_id;
    }

    public String getAuthority_name() {
        return authority_name;
    }
    public void setAuthority_name(String authority_name) {
        this.authority_name = authority_name;
    }

    public long getDeleted_at() {
        return deleted_at;
    }
    public void setDeleted_at(Long deleted_at) {
        this.deleted_at = deleted_at;
    }
}